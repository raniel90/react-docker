import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Minha Primeira App Dockerizada
        </p>
        <p
        >
          Engenharia de Software
        </p>
      </header>
    </div>
  );
}

export default App;
